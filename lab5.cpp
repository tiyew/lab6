/******************
  Tiye Watson
  tiyew
  Lab 5
  Lab Section: 3
  Nushrat Humaira
******************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;

enum Suit { SPADES = 0, HEARTS = 1, DIAMONDS = 2, CLUBS = 3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  /* This is to seed the random generator */
  srand(unsigned (time(0)));

   /* Write a for loop that initializes the deck to a standard hand of 52
    *cards for each card suit */
   int i;
   int y;
   int x = 0;

   Card deck[52];

   for(y = 0; y <= 3; y++) {
     for(i = 2; i <= 14; i++) {
       deck[x].value = i;
       deck[x].suit = static_cast<Suit>(y);
       x++;
     }
   }

  /* After the deck is created and initialzed, random_shuffle() is called to
    *randomly shuffle the deck of cards built */
   random_shuffle(&deck[0], &deck[52], myrandom);

  /* Initialize hand of 5 cards from the first five cards of the deck created */
    Card hand[5] = {deck[0], deck[1], deck[2], deck[3], deck[4]};

    /* Sort the cards from the hand that was built */
     sort(&hand[0], &hand[5], suit_order);

    /* Print the hand below. You will use the functions get_card_name and
       *get_suit_code */
     /* use a for loop that prints out the randomly given hand, using the
        *function get_car_name and get_suit_code */
     for(int a = 0; a < 5; a++) {
     cout << right << setw(10) << get_card_name(hand[a]);
     cout << get_suit_code(hand[a]) << endl;
   }
  return 0;
}


/* This function will be passed to the sort funtion. If the left suit is less
   *than the right suit, it returns true. If the left suit and right suit
    *is equal to one another, then it looks to see if the left value
    *is less than the right suit, it returns true. Else, it returns false. */
bool suit_order(const Card& lhs, const Card& rhs) {
  if(lhs.suit < rhs.suit) {
    return true;
  }
  else if(lhs.suit == rhs.suit) {
    if(lhs.value < rhs.value) {
      return true;
    }
  }
  return false;
}

// Function that returns the UTF -8 code for the card's suit
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

// Function that returns the string name of the card's value
string get_card_name(Card& c) {
  switch (c.value) {
    case 2:    return "2 of ";
    case 3:    return "3 of ";
    case 4:    return "4 of ";
    case 5:    return "5 of ";
    case 6:    return "6 of ";
    case 7:    return "7 of ";
    case 8:    return "8 of ";
    case 9:    return "9 of ";
    case 10:   return "10 of ";
    case 11:   return "Jack of ";
    case 12:   return "Queen of ";
    case 13:   return "King of ";
    case 14:   return "Ace of ";
    default:   return "";
  }
}
